package engineer.fernando.cms.tracks.handler;

import engineer.fernando.cms.domainmodel.avro.Position;
import engineer.fernando.cms.domainmodel.avro.SensorTrackType;
import engineer.fernando.cms.tracks.handler.model.SystemTrackBuilder;
import engineer.fernando.cms.tracks.handler.model.TrackTacticalSituationBuilder;
import engineer.fernando.cms.tracks.handler.model.entity.SystemTrack;
import engineer.fernando.cms.tracks.handler.model.entity.TrackSpecification;
import engineer.fernando.cms.tracks.handler.model.entity.TrackTacticalSituation;
import engineer.fernando.cms.tracks.handler.repository.SystemTrackRepository;
import engineer.fernando.cms.tracks.handler.repository.TrackTacticalSituationRepository;
import engineer.fernando.cms.tracks.handler.util.TrackSpecificationBundle;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

@ActiveProfiles("test")
@DataJpaTest
class SystemTrackRepositoryTests {

    @Autowired
    SystemTrackRepository systemTrackRepository;
    @Autowired
    TrackTacticalSituationRepository trackTacticalSituationRepository;
    
    static SystemTrack track;
    static SystemTrack weapon;
    static Position position;
    static Position anotherPosition;
    static TrackTacticalSituation tacticalUpdate;
    static TrackTacticalSituation anotherTacticalUpdate;

    @BeforeAll
    static void beforeAll() {

        position = new Position(0.98,2.3,0.0);
        anotherPosition = new Position(1.0,2.2,0.0);
        double speed = 5;

    }

    @Test
    void testEmptyRepository() {
        List<SystemTrack> ships = systemTrackRepository.findAll();
        assertTrue(ships.isEmpty());
    }

    @Test
    void testRepository() {
        // Build specification from Ship template.
        TrackSpecification shipSpecification =
                TrackSpecificationBundle.buildTrackSpecification(SensorTrackType.OWNSHIP);

        track = new SystemTrackBuilder()
                .setPlayerId(31415).setSensorTrackType(SensorTrackType.OWNSHIP).setName("Antares")
                .setWeaponsAvailable(5).setOrderedSpeedOverGround(15.3).setOrderedCourseOverGround(314)
                .setTrackTechSpec(shipSpecification).setHit(false).setSystemTrackId(BigDecimal.valueOf(1))
                .build();
        // 1st track publication
        SystemTrack ownShip = systemTrackRepository.save(track);
        // I - systemTrackRepository populated
        assertEquals("O1P31415", ownShip.getSystemTrackId());
        // II - systemTrackRepository findBySystemTrackId working
        Optional<SystemTrack> optionalSystemTrack = systemTrackRepository.findBySystemTrackId("O1P31415");
        assertTrue(optionalSystemTrack.isPresent());
        SystemTrack foundSystemTrack = optionalSystemTrack.get();
        assertEquals(5, foundSystemTrack.getTrackSpecification().getWeaponsCapacity());

        // Adding first tactical update for track
        tacticalUpdate = new TrackTacticalSituationBuilder().setSystemTrack(foundSystemTrack)
                .setLatitude(position.getLatitude()).setLongitude(position.getLongitude())
                .setCourseOverGround(123.4).setSpeedOverGround(31.4).setdBNoise(20.5).setFuelInGallons(45.8).build();
        // 1st tactical update publication
        TrackTacticalSituation update1 = trackTacticalSituationRepository.save(tacticalUpdate);
        // III - trackTacticalSituationRepository populated
        assertEquals("O1P31415", update1.getSystemTrack().getSystemTrackId());
        assertEquals(position.getLongitude(), update1.getLongitude(),0.00001);
        // IV - trackTacticalSituationRepository findBySystemTrack working
        List<TrackTacticalSituation> tacticalUpdatesperSystemTrack = trackTacticalSituationRepository.findBySystemTrack(track);
        assertEquals(1, tacticalUpdatesperSystemTrack.size());
        assertEquals(position.getLatitude(), tacticalUpdatesperSystemTrack.get(0).getLatitude(),0.0001);

        //V - pulling again foundAgainSystemTrack from systemTrackRepository contains the update even track was not saved
        Optional<SystemTrack> optionalSystemTrackAgain = systemTrackRepository.findBySystemTrackId("O1P31415");
        SystemTrack foundAgainSystemTrack = optionalSystemTrackAgain.get();
        assertEquals(1, foundAgainSystemTrack.getTacticalUpdates().size());
        assertEquals(position.getLatitude(), foundAgainSystemTrack.getTacticalUpdates().get(0).getLatitude(),0.0001);

        // 2nd tactical update build
        anotherTacticalUpdate = new TrackTacticalSituationBuilder().setSystemTrack(track)
                .setLatitude(anotherPosition.getLatitude()).setLongitude(anotherPosition.getLongitude())
                .setCourseOverGround(223.4).setSpeedOverGround(27.1).setdBNoise(18.2).setFuelInGallons(98.8).build();

        //VI - pulling again foundOutdatedSystemTrack from systemTrackRepository contains the 2nd update despite its not saved yet
        SystemTrack foundOutdatedSystemTrack = systemTrackRepository.findBySystemTrackId("O1P31415").get();
        assertEquals(2, foundOutdatedSystemTrack.getTacticalUpdates().size());

        // VII - And trackTacticalSituationRepository contains also the 2nd update despite its not saved yet (Cascade!)
        List<TrackTacticalSituation> tacticalUpdatesperSystemTrackNewPooling = trackTacticalSituationRepository.findBySystemTrack(track);
        assertEquals(2, tacticalUpdatesperSystemTrackNewPooling.size());

        // 2nd tactical update publication
        TrackTacticalSituation update2 = trackTacticalSituationRepository.save(anotherTacticalUpdate);

        //VIII - After save, trackTacticalSituationRepository still contains both updates
        // (Resume: Dont need to save at child side due to the Cascade annotation on entity )
        List<TrackTacticalSituation> tacticalUpdatesperSystemTrackAnotherNewPooling = trackTacticalSituationRepository.findBySystemTrack(track);
        assertEquals(2, tacticalUpdatesperSystemTrackAnotherNewPooling.size());

        // IX - trackTacticalSituationRepository findBySystemTrack working
        Optional<TrackTacticalSituation> optLastUpdatesperSystemTrack = trackTacticalSituationRepository
                .findLastUpdateBySystemTrack(track);
        assertTrue(optLastUpdatesperSystemTrack.isPresent());
        TrackTacticalSituation lastUpdatesperSystemTrack = optLastUpdatesperSystemTrack.get();
        assertEquals(anotherPosition.getLatitude(), lastUpdatesperSystemTrack.getLatitude(),0.0001);
        assertEquals(98.8, lastUpdatesperSystemTrack.getFuelInGallons(),0.0001);


        // 2nd track built
        // Build specification from Weapon template.
        TrackSpecification weaponSpecification =
                TrackSpecificationBundle.buildTrackSpecification(SensorTrackType.WEAPON);

        weapon = new SystemTrackBuilder()
                .setPlayerId(31415).setSensorTrackType(SensorTrackType.WEAPON).setName("LittleJoe")
                .setWeaponsAvailable(0).setOrderedSpeedOverGround(1500.3).setOrderedCourseOverGround(180.7)
                .setTrackTechSpec(weaponSpecification).setHit(false).setSystemTrackId(BigDecimal.valueOf(2))
                .build();

        // 2nd Publication
        SystemTrack torpille = systemTrackRepository.save(weapon);
        assertEquals("W2P31415", torpille.getSystemTrackId());
        optionalSystemTrack = systemTrackRepository.findBySystemTrackId("W2P31415");
        assertTrue(optionalSystemTrack.isPresent());
        foundSystemTrack = optionalSystemTrack.get();
        assertEquals(0, foundSystemTrack.getTrackSpecification().getWeaponsCapacity());

    }
}

