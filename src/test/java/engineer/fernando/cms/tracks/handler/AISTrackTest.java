package engineer.fernando.cms.tracks.handler;

import engineer.fernando.ais.decoder.messages.AISMessage;
import engineer.fernando.ais.decoder.messages.PositionReportClassAScheduled;
import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.cms.tracks.handler.model.entity.AISTrack;
import engineer.fernando.cms.tracks.handler.service.AISMessageDecoderService;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

import static junit.framework.Assert.assertEquals;

class AISTrackTest {

	private List<String> nmeaMessages = Arrays.asList(
		"!AIVDM,2,1,9,B,59NRrG@25UtPDhiOH00q21<D@<v3O3H00000,0*79",
		"!AIVDM,2,2,9,B,001J2PtDe55P0wDSkPhA3l`0Pp000000000,2*5F",
		"!AIVDM,1,1,,B,19NS7Sp02wo?HETKA2K6mUM20<L=,0*27"
	);

	private AISMessageDecoderService aisMessageDecoderService = new AISMessageDecoderService();

	List<AISMessage> decodedMessages = aisMessageDecoderService.decode(nmeaMessages);

	@Test
	void testAISTrackFactory() throws ClassNotFoundException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
		AISMessage aisMessageShipAndVoyageData = decodedMessages.get(0);
		System.out.println(String.format("Decoded AISMessage: %s ", aisMessageShipAndVoyageData));
		AISTrack aisTrack = AISTrack.aisTrackfactory(aisMessageShipAndVoyageData);
		assertEquals("MessageType", AISMessageType.SHIP_AND_VOYAGE_RELATED_DATA, aisTrack.getMessageType());
		assertEquals("MMSI", 636009053, aisTrack.getMMSI());
		assertEquals("Ship Name", "NP SEDCO 706", aisTrack.getShipName());

		AISMessage aisMessagePositionReportClassAScheduled = decodedMessages.get(1);
		System.out.println(String.format("Decoded AISMessage: %s ", aisMessagePositionReportClassAScheduled));
		aisTrack = AISTrack.aisTrackfactory(aisMessagePositionReportClassAScheduled);
		assertEquals("MessageType", AISMessageType.POSITION_REPORT_CLASS_A_SCHEDULED,
			aisTrack.getMessageType());
		assertEquals("MMSI", 636012431,aisTrack.getMMSI());
		assertEquals("Lng", -122.469253, aisTrack.getLongitude(),0.000001);
		assertEquals("Lat", 47.651165, aisTrack.getLatitude(),0.000001);

		//Now lets simulate an AISMessage update for the same ship and test the AISTrack update
		Field aisTrackMessageLatField = PositionReportClassAScheduled.class.getSuperclass().getDeclaredField("latitude");
		aisTrackMessageLatField.setAccessible(true);
		aisTrackMessageLatField.set(aisMessagePositionReportClassAScheduled,48f);
		Field aisTrackMessageLngField = PositionReportClassAScheduled.class.getSuperclass().getDeclaredField("longitude");
		aisTrackMessageLngField.setAccessible(true);
		aisTrackMessageLngField.set(aisMessagePositionReportClassAScheduled,-123f);
		aisTrack.updateAISTrack(aisMessagePositionReportClassAScheduled);

		assertEquals("MessageType", AISMessageType.POSITION_REPORT_CLASS_A_SCHEDULED,
			aisTrack.getMessageType());
		assertEquals("MMSI", 636012431,aisTrack.getMMSI());
		assertEquals("Lng", -123, aisTrack.getLongitude(),0.000001);
		assertEquals("Lat", 48, aisTrack.getLatitude(),0.000001);

	}
}
