package engineer.fernando.cms.tracks.handler;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.CommunicationState;
import engineer.fernando.ais.decoder.messages.types.SyncState;
import engineer.fernando.cms.tracks.handler.model.entity.AISTrack;
import engineer.fernando.cms.tracks.handler.model.entity.AISTrackMessage;
import engineer.fernando.cms.tracks.handler.repository.AISTrackMessageRepository;
import engineer.fernando.cms.tracks.handler.repository.AISTrackRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;


@ActiveProfiles("test")
@DataJpaTest
 class AISTrackMessageRepositoryTests {

    static AISTrackMessage aisTrack1979;
    static AISTrackMessage aisTrack1977;
    static AISTrackMessage aisTrack1977Update2;
    static AISTrackMessage aisTrack1977Update3;

    @Autowired
    AISTrackMessageRepository aisTrackMessageRepository;


    @BeforeAll
    static void beforeAll() {
        CommunicationState communicationState = new CommunicationState(SyncState.UTC_DIRECT) {
            @Override
            public SyncState getSyncState() {
                return super.getSyncState();
            }
        };

        aisTrack1979 = new AISTrackMessage();
        aisTrack1979.setMessageType(AISMessageType.SHIP_AND_VOYAGE_RELATED_DATA);
        aisTrack1979.setCommunicationState(communicationState);
        aisTrack1979.setMMSI(1979);
        aisTrack1979.setDestination("Prosperity");

        aisTrack1977 = new AISTrackMessage();
        aisTrack1977.setMessageType(AISMessageType.POSITION_REPORT_CLASS_A_SCHEDULED);
        aisTrack1977.setMMSI(1977);
        aisTrack1977.setLatitude(-23.2f);
        aisTrack1977.setLongitude(-43.4f);
        aisTrack1977.setCommunicationState(communicationState);

        aisTrack1977Update2 = new AISTrackMessage();
        aisTrack1977Update2.setMessageType(AISMessageType.POSITION_REPORT_CLASS_A_SCHEDULED);
        aisTrack1977Update2.setCommunicationState(communicationState);
        aisTrack1977Update2.setMMSI(1977);
        aisTrack1977Update2.setLatitude(-23.1f);
        aisTrack1977Update2.setLongitude(-43.3f);

        aisTrack1977Update3 = new AISTrackMessage();
        aisTrack1977Update3.setMessageType(AISMessageType.POSITION_REPORT_CLASS_A_SCHEDULED);
        aisTrack1977Update3.setCommunicationState(communicationState);
        aisTrack1977Update3.setMMSI(1977);
        aisTrack1977Update3.setLatitude(-23.0f);
        aisTrack1977Update3.setLongitude(-43.2f);
    }

    @BeforeEach
    void setUp() {

    }

    @Test
    void testEmptyRepository() {
        List<AISTrackMessage> aisTrackMessages = aisTrackMessageRepository.findAll();
        assertTrue(aisTrackMessages.isEmpty());
    }

    @Test
    void testRepository() {
        // 1st Publication : SHIP_AND_VOYAGE_RELATED_DATA for MMSI 1979
        AISTrackMessage aisTrackMessage = aisTrackMessageRepository.save(aisTrack1979);
        assertEquals(1979,aisTrackMessage.getMMSI());
        assertEquals(AISMessageType.SHIP_AND_VOYAGE_RELATED_DATA,aisTrackMessage.getMessageType());
        assertEquals("Prosperity",aisTrackMessage.getDestination());
        assertEquals(1, aisTrackMessageRepository.count());

        // 2nd Publication : POSITION_REPORT_CLASS_A_SCHEDULED for MMSI 1977
        AISTrackMessage anotherAisTrackMessage = aisTrackMessageRepository.save(aisTrack1977);
        assertEquals(1977,anotherAisTrackMessage.getMMSI());
        assertEquals(AISMessageType.POSITION_REPORT_CLASS_A_SCHEDULED,anotherAisTrackMessage.getMessageType());
        assertEquals(-23.2f,anotherAisTrackMessage.getLatitude(),0.01);
        assertEquals(-43.4f,anotherAisTrackMessage.getLongitude(),0.01);
        assertEquals(2, aisTrackMessageRepository.count());

        // 3rd Publication : Another POSITION_REPORT_CLASS_A_SCHEDULED for MMSI 1977
        AISTrackMessage anotherAisTrackMessage2 = aisTrackMessageRepository.save(aisTrack1977Update2);
        assertEquals(1977,anotherAisTrackMessage2.getMMSI());
        assertEquals(-23.1f,anotherAisTrackMessage2.getLatitude(),0.01);
        assertEquals(3, aisTrackMessageRepository.count());

        // 4th Publication : Another POSITION_REPORT_CLASS_A_SCHEDULED for MMSI 1977
        AISTrackMessage anotherAisTrackMessage3 = aisTrackMessageRepository.save(aisTrack1977Update3);
        assertEquals(1977,anotherAisTrackMessage3.getMMSI());
        assertEquals(-23.0f,anotherAisTrackMessage3.getLatitude(),0.01);
        assertEquals(4, aisTrackMessageRepository.count());

        //Deleting updates from ship mmsi 1977 shall no remain for 1977
        aisTrackMessageRepository.deleteByMmsi(1977L);
        // , and remains only 1 update (for 1979)
        assertEquals(1, aisTrackMessageRepository.count());
    }

}
