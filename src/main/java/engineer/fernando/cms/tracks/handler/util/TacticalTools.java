package engineer.fernando.cms.tracks.handler.util;

import java.awt.geom.Point2D;
import org.apache.commons.lang3.tuple.Pair;
import org.geotools.referencing.GeodeticCalculator;
import org.geotools.referencing.crs.DefaultGeographicCRS;


public class TacticalTools {

    private static final double NAUTICAL_MILES_PER_METERS = 54 * 10E-5;
    private static final double MPS_PER_KNOT = 514.444444444444 * 10E-3;

    private TacticalTools() {
    }

    /**
     * Compute projected point in a future time and the distance to this point given course and speed from last update
     * @param currentLatLong
     * @param currentCourseAndSpeed
     * @param timeInSeconds
     * @return a Pair of futurepoint (Point2D) and distance travelled in nautical miles
     */
    public static Pair<Point2D, Double> computeProjectionAndDistance(Pair<Double,Double> currentLatLong, Pair<Double,Double> currentCourseAndSpeed, long timeInSeconds) {
        double latitude = currentLatLong.getLeft();
        double longitude = currentLatLong.getRight();
        double course = currentCourseAndSpeed.getLeft();
        double speed = currentCourseAndSpeed.getRight();
        GeodeticCalculator gc = new GeodeticCalculator(DefaultGeographicCRS.WGS84);
        gc.setStartingGeographicPoint(longitude,latitude);
        double distanceTravelled =
                (TacticalTools.knotsToMetersPerSecond(speed) * timeInSeconds);
        gc.setDirection(TacticalTools.courseInDegreesToAzimuth(course),distanceTravelled);
        Point2D futurePoint = gc.getDestinationGeographicPoint();
        return Pair.of(futurePoint,metersToNauticalMiles(distanceTravelled));
    }

    /**
     *
     * @param speedInKnots
     * @return
     */
    public static double knotsToMetersPerSecond(double speedInKnots) {
        return speedInKnots * MPS_PER_KNOT;
    }

    public static double metersToNauticalMiles(double distanceInMeters) {
        return distanceInMeters * NAUTICAL_MILES_PER_METERS;
    }

    /**
     *
     * @param courseInDegrees
     * @return
     */
    public static double courseInDegreesToAzimuth(double courseInDegrees) {
        double azimuth;
        if (courseInDegrees > 180.0) {
            azimuth = -180.0 + (courseInDegrees - 180.0);
        } else if(courseInDegrees < -180){
            azimuth = courseInDegrees + 360;
        }
        else {
            azimuth = courseInDegrees;
        }
        return azimuth;
    }

    /**
     *
     * @param azimuth
     * @return
     */
    public static double azimuthToCourseInDegrees(double azimuth) {
        double courseInDegrees;
        if (azimuth < 0.0) {
            courseInDegrees = 360.0 + azimuth;
        } else {
            courseInDegrees = azimuth;
        }
        return courseInDegrees;
    }

    /**
     *
     * @param courseInDegrees
     * @return
     */
    public static double normalizeCourseInDegrees(double courseInDegrees) {
        if (courseInDegrees > 360) {
            courseInDegrees -= 360;
        }
        if (courseInDegrees < 0) {
            courseInDegrees += 360;
        }
        return courseInDegrees;
    }

    private static String decimalToDMS(double decimal) {
        String signal = decimal < 0 ? "-" : "+";
        double absDecimal = Math.abs(decimal);
        int integerPart = (int) absDecimal;
        double residue = absDecimal % 1;
        String degrees = String.valueOf(integerPart);
        double minDecimal = residue * 60;
        residue = minDecimal % 1;
        integerPart = (int) minDecimal;
        String min = String.valueOf(integerPart);
        double secDecimal = residue * 60;
        integerPart = (int) secDecimal;
        String sec = String.valueOf(integerPart);
        return signal + degrees + "°" + min + "'" + sec + "\"";
    }

    public static String formatLatLng(double latitude, double longitude){
        String lat = decimalToDMS(latitude) + " ";
        lat = lat.concat(latitude > 0 ? "N" : "S");
        String lng = decimalToDMS(longitude) + " ";
        lng = lng.concat(latitude > 0 ? "E" : "W");
        return lat.concat(", ").concat(lng);
    }
}