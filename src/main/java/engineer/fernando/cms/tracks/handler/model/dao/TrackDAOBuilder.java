package engineer.fernando.cms.tracks.handler.model.dao;

import engineer.fernando.cms.domainmodel.avro.SensorTrackType;
import engineer.fernando.cms.tracks.handler.util.TacticalTools;
import org.apache.commons.lang3.tuple.Pair;

import java.awt.geom.Point2D;

public class TrackDAOBuilder {

    private SensorTrackType sensorTrackType;
    private long playerId;
    private String name;
    private double orderedSpeedOverGround;
    private double orderedCourseOverGround;
    private int weaponsAvailable;
    private boolean hit;
    private double latitude;
    private double longitude;
    private double speedOverGround;
    private double courseOverGround;
    private double distanceTravelled;
    private double fuelInGallons;
    private double dBNoise;
    private String trackId;
    private long updatedTime;
    private long creationTime;
    private boolean radarAntennaTurnStatus;
    private boolean radarStatus;
    private double radarAzimuth;

    public TrackDAOBuilder setTrackId(String trackId) {
        this.trackId = trackId;
        return this;
    }

    public TrackDAOBuilder setSensorTrackType(SensorTrackType sensorTrackType) {
        this.sensorTrackType = sensorTrackType;
        return this;
    }

    public TrackDAOBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public TrackDAOBuilder setPlayerId(long playerId) {
        this.playerId = playerId;
        return this;
    }

    public TrackDAOBuilder setOrderedCourseOverGround(double orderedCourseOverGround) {
        this.orderedCourseOverGround = orderedCourseOverGround;
        return this;
    }

    public TrackDAOBuilder setOrderedSpeedOverGround(Double orderedSpeedOverGround) {
        this.orderedSpeedOverGround = orderedSpeedOverGround;
        return this;
    }

    public TrackDAOBuilder setWeaponsAvailable(int weaponsAvailable) {
        this.weaponsAvailable = weaponsAvailable;
        return this;
    }

    public TrackDAOBuilder setHit(boolean hit) {
        this.hit = hit;
        return this;
    }

    public TrackDAOBuilder setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public TrackDAOBuilder setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public TrackDAOBuilder setSpeedOverGround(double speedOverGround) {
        this.speedOverGround = speedOverGround;
        return this;
    }

    public TrackDAOBuilder setCourseOverGround(double courseOverGround) {
        this.courseOverGround = courseOverGround;
        return this;
    }

    public TrackDAOBuilder setDistanceTravelled(double distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
        return this;
    }

    public TrackDAOBuilder setFuelInGallons(double fuelInGallons)
    {
        this.fuelInGallons = fuelInGallons;
        return this;
    }

    public TrackDAOBuilder setNoiseInDb(double dBNoise) {
        this.dBNoise = dBNoise;
        return this;
    }

    public TrackDAOBuilder setCreationTime(long creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public TrackDAOBuilder setUpdatedTime(long updatedTime) {
        this.updatedTime = updatedTime;
        return this;
    }

    public TrackDAOBuilder setRadarAntennaTurnStatus(boolean radarAntennaTurnStatus) {
        this.radarAntennaTurnStatus = radarAntennaTurnStatus;
        return this;
    }

    public TrackDAOBuilder setRadarStatus(boolean radarStatus) {
        this.radarStatus = radarStatus;
        return this;
    }

    public TrackDAOBuilder setRadarAzimuth(double radarAzimuth) {
        this.radarAzimuth = radarAzimuth;
        return this;
    }


    public TrackDAO build(){
        TrackDAO track = new TrackDAO();
        track.setTrackId(this.trackId);
        track.setSensorTrackType(this.sensorTrackType);
        track.setName(this.name);
        track.setPlayerId(this.playerId);
        track.setLatitude(this.latitude);
        track.setLongitude(this.longitude);
        track.setSpeedOverGround(this.speedOverGround);
        track.setCourseOverGround(this.courseOverGround);
        Pair<Double,Double> currentLatLong = Pair.of(this.latitude, this.longitude);
        Pair<Double,Double> currentCourseAndSpeed = Pair.of(this.courseOverGround, this.speedOverGround);
        //TODO: Include param to configure projection time from frontend to backend.
        long timeInSeconds;
        switch (track.getSensorTrackType()){
            case WEAPON:
                timeInSeconds = 60L;
                break;
            case OWNSHIP: default:
                timeInSeconds = 60L * 60;
                break;
        }
        Pair<Point2D, Double> projectionDefault =
                TacticalTools.computeProjectionAndDistance(
                        currentLatLong,
                        currentCourseAndSpeed,
                        timeInSeconds
                );
        track.setProjetedLatitude(projectionDefault.getLeft().getY());
        track.setProjectedLongitude(projectionDefault.getLeft().getX());
        track.setOrderedSpeedOverGround(this.orderedSpeedOverGround);
        track.setOrderedCourseOverGround(this.orderedCourseOverGround);
        track.setDistanceTravelled(this.distanceTravelled);
        track.setWeaponsAvailable(this.weaponsAvailable);
        track.setRadarAntennaTurnStatus(this.radarAntennaTurnStatus);
        track.setRadarAntennaTurnStatus(this.radarAntennaTurnStatus);
        track.setRadarStatus(this.radarStatus);
        track.setRadarAzimuth(this.radarAzimuth);
        track.setHit(this.hit);
        track.setCreationTime(this.creationTime);
        track.setUpdatedTime(this.updatedTime);
        track.setFuelInGallons(this.fuelInGallons);
        track.setdBNoise(this.dBNoise);
        return track;
    }

}
