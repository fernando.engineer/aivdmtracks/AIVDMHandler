package engineer.fernando.cms.tracks.handler.model;

public class Detection {

    private String trackIdSource;
    private long creationTime;
    private double distance;
    private double latitude;
    private double longitude;
    private double azimuth;
    private double dBNoise;
    private boolean mySources;

    public Detection(String trackIdSource, double latitude, double longitude, double distance, double azimuth, double dBNoise) {
        this.trackIdSource = trackIdSource;
        this.creationTime = System.currentTimeMillis();
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = distance;
        this.azimuth = azimuth;
        this.dBNoise = dBNoise;
        this.mySources = false;
    }

    public String getTrackIdSource() {
        return trackIdSource;
    }

    public void setTrackIdSource(String trackIdSource) {
        this.trackIdSource = trackIdSource;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public double getAzimuth() {
        return azimuth;
    }

    public void setAzimuth(float azimuth) {
        this.azimuth = azimuth;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getdBNoise() {
        return dBNoise;
    }

    public void setdBNoise(double dBNoise) {
        this.dBNoise = dBNoise;
    }
    public boolean isMySources() {
        return mySources;
    }

    public void setMySources(boolean mySources) {
        this.mySources = mySources;
    }

    @Override
    public String toString() {
        return "Detection [trackIdSource=" + trackIdSource + ", distance=" + distance
                + ", Lat/Lng=" + latitude + "," + longitude + ",azimuth="+ azimuth
                + ", NoiseInDb=" +dBNoise + ", MySource?:" +mySources + ", CreationTime=" +creationTime +" received.]";
    }

}
