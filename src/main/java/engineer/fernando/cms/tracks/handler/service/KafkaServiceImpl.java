package engineer.fernando.cms.tracks.handler.service;

import engineer.fernando.cms.tracks.handler.messages.KafkaJsonDeserializer;
import engineer.fernando.cms.tracks.handler.messages.KafkaJsonSerializer;
import engineer.fernando.cms.tracks.handler.messages.TrackHandlerMessage;
import engineer.fernando.cms.tracks.handler.model.dao.AISTrackInfoDAO;
import engineer.fernando.cms.tracks.handler.model.dao.TrackDAO;
import engineer.fernando.cms.tracks.handler.model.dao.TrackPerformanceDAO;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.config.TopicConfig;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.apache.kafka.clients.admin.AdminClient;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.kafka.receiver.KafkaReceiver;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.receiver.ReceiverRecord;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderOptions;
import reactor.kafka.sender.SenderRecord;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

@Service
public class KafkaServiceImpl implements KafkaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaServiceImpl.class);

    private static final String KAFKA_CONFIGURATION_FILE = "kafka.properties";

    private int msxIdx = 1;

    private Flux<ReceiverRecord<String, TrackHandlerMessage>> kafkaStream;

    private KafkaSender<Integer, TrackHandlerMessage> sender;

    @Value("${spring.kafka.topic.track}")
    private String topicTrack;
    @Value("${spring.kafka.topic.track.performance}")
    private String topicTrackPerformance;
    @Value("${spring.kafka.topic.alert.tactical}")
    private String topicTacticalAlert;
    @Value("${spring.kafka.topic.nmea}")
    private String topicAISMessage;

    private List<String> topics;

    @PostConstruct
    private void init() throws IOException, ExecutionException, InterruptedException {
        topics = new ArrayList<>(Arrays.asList(
            topicTrack,
            topicTrackPerformance,
            topicTacticalAlert,
            topicAISMessage));
        Properties adminProperties = PropertiesLoaderUtils.loadAllProperties(KAFKA_CONFIGURATION_FILE);
        try (AdminClient admin = AdminClient.create(adminProperties)) {
            ListTopicsResult listTopics = admin.listTopics();
            Set<String> existingTopicNames = listTopics.names().get();
            for (String topic : topics) {
                if(existingTopicNames.contains(topic)){
                    continue;
                }
                Map<String, String> configs = new HashMap<>();
                configs.put(TopicConfig.RETENTION_MS_CONFIG, "1250");
                int partitions = 1;
                short replicationFactor = 1;
                NewTopic newTopic =
                    new NewTopic(topic, partitions, replicationFactor)
                        .configs(configs);
                CreateTopicsResult result = admin.createTopics(
                    Collections.singleton(newTopic)
                );
                KafkaFuture<Void> future = result.values().get(topic);
                future.get();
            }
        }

        Properties producerProperties = PropertiesLoaderUtils.loadAllProperties(KAFKA_CONFIGURATION_FILE);
        producerProperties.put(ProducerConfig.ACKS_CONFIG, "all");
        producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class);
        producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaJsonSerializer.class);
        SenderOptions<Integer, TrackHandlerMessage> senderOptions = SenderOptions.create(producerProperties);
        sender = KafkaSender.create(senderOptions);

        Properties consumerProperties = PropertiesLoaderUtils.loadAllProperties(KAFKA_CONFIGURATION_FILE);
        consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, "trackhandler-group");
        consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaJsonDeserializer.class);
        consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        ReceiverOptions<String, TrackHandlerMessage> receiverOptions = ReceiverOptions.create(consumerProperties);
        kafkaStream = createTopicCache(receiverOptions);

        if(LOGGER.isInfoEnabled()){
            LOGGER.info("TRACKHANDLER STARTED!");
        }
        sendAlertMessage("*", "TRACKHANDLER RUNNING.");
    }

    @PreDestroy
    private void end(){
        if(LOGGER.isInfoEnabled()){
            LOGGER.info("SystemTrack Service shutdown!");
        }
        sendAlertMessage("*", "TRACKHANDLER SHUTDOWN.");
    }

    public Flux<ReceiverRecord<String, TrackHandlerMessage>> getKafkaStream() {
        return kafkaStream;
    }

    public void sendTrackDAOMessage(List<TrackDAO> trackDAOList){
        String trackId = trackDAOList.size() + " Tr(S)";
        sendMessage(new TrackHandlerMessage(topicTrack,trackId,trackDAOList));
    }


    public void sendTrackPerformanceDAOMessage(List<TrackPerformanceDAO> trackPerformanceDAOList) {
        String trackId = trackPerformanceDAOList.size() + " Tr(S)";
        sendMessage(new TrackHandlerMessage(topicTrackPerformance,trackId,trackPerformanceDAOList));
    }

    public void sendAlertMessage(String trackId, String message) {
        sendMessage(new TrackHandlerMessage(topicTacticalAlert,trackId,message));
    }

    public void sendAISTrack(AISTrackInfoDAO aisTrackInfo){
        String trackId = String.valueOf(aisTrackInfo.getAisTrack().getMMSI());
        sendMessage(new TrackHandlerMessage(topicAISMessage,trackId,aisTrackInfo));
    }

    private void sendMessage(TrackHandlerMessage trackHandlerMessage){
        int count = 1;
        CountDownLatch latch = new CountDownLatch(count);
        sendGenericMessagesBatch(trackHandlerMessage.getTopic(),trackHandlerMessage, count,latch);
    }

    private void sendGenericMessagesBatch(String topic, TrackHandlerMessage message, int count, CountDownLatch latch) {
        sender.send(Flux.range(1, count)
                   .map(i -> SenderRecord.create(
                           new ProducerRecord<>(topic, msxIdx, message), i))
            )
            .doOnError(e -> {
                if(LOGGER.isErrorEnabled()){
                    LOGGER.error("Send failed", e);
                }
            })
            .subscribe(r -> {
                RecordMetadata metadata = r.recordMetadata();
                Instant timestamp = Instant.ofEpochMilli(metadata.timestamp());
                if(LOGGER.isDebugEnabled()){
                    LOGGER.debug(String.format("Kafka %s topic message %s sent successfully, topic-partition=%s-%d offset=%d timestamp=%s",
                        topic,
                        r.correlationMetadata(),
                        metadata.topic(),
                        metadata.partition(),
                        metadata.offset(),
                        timestamp.toString()));
                }
                latch.countDown();
                msxIdx++;
            });
    }

    private <T,G> Flux<ReceiverRecord<T,G>> createTopicCache(ReceiverOptions<T, G> receiverOptions){
        ReceiverOptions<T,G> options = receiverOptions.subscription(topics);
        return KafkaReceiver.create(options).receive().cache(Duration.ofMillis(60000));
    }

}