package engineer.fernando.cms.tracks.handler.messages;

public class TrackCmd {
    private String trackId;
    private String command;
    private double value;

    public String getTrackId() {
        return trackId;
    }

    public String getCommand() {
        return command;
    }

    public double getValue() {
        return value;
    }
}
