package engineer.fernando.cms.tracks.handler.service;

import engineer.fernando.ais.decoder.exceptions.NMEAParseException;
import engineer.fernando.ais.decoder.messages.AISMessage;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import engineer.fernando.ais.decoder.nmea.NMEAMessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

@Service
public class AISMessageDecoderService implements Consumer<AISMessage> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AISMessageDecoderService.class);
    private List<AISMessage> aisMessages = new LinkedList<>();

    public List<AISMessage> decode(List<String> nmeaMessagesAsStrings) {
        aisMessages = new LinkedList<>();
        NMEAMessageHandler nmeaMessageHandler = new NMEAMessageHandler("http", this);

        // Decode all received messages
        nmeaMessagesAsStrings.forEach(nmeaMessageAsString -> {
            try {
                NMEAMessage nmeaMessage = NMEAMessage.fromString(nmeaMessageAsString);
                nmeaMessageHandler.accept(nmeaMessage);
            } catch(NMEAParseException e) {
                if(LOGGER.isErrorEnabled()){
                    LOGGER.error(new StringBuilder().append("NMEAParseException: ").append(e.getMessage()).toString());
                }
            }
        });

        // Flush receiver for unparsed message fragments
        List<NMEAMessage> unparsedMessages = nmeaMessageHandler.flush();
        unparsedMessages.forEach(unparsedMessage -> {
            if(LOGGER.isErrorEnabled()){
                LOGGER.error(new StringBuilder().append("NMEA message not used: ").append(unparsedMessage).toString());
            }
        });

        // Return result
        return aisMessages;
    }

    @Override
    public void accept(AISMessage aisMessage) {
        aisMessages.add(aisMessage);
    }
}
