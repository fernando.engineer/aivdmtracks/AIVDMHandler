package engineer.fernando.cms.tracks.handler.repository;


import engineer.fernando.cms.tracks.handler.model.entity.AISTrack;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface AISTrackRepository extends JpaRepository<AISTrack,Long> {

    Optional<AISTrack> findByMmsi(long mmsi);


    @Transactional
    @Modifying
    @Query("DELETE FROM AISTrack c WHERE c.mmsi = :mmsi")
    void deleteByMmsi(long mmsi);
}
