package engineer.fernando.cms.tracks.handler.service;

import engineer.fernando.cms.domainmodel.avro.SensorTrackType;
import engineer.fernando.cms.domainmodel.avro.Track;
import engineer.fernando.cms.domainmodel.util.Codec;
import engineer.fernando.cms.tracks.handler.exception.TrackHandlerCommandException;
import engineer.fernando.cms.tracks.handler.model.Detection;
import engineer.fernando.cms.tracks.handler.model.SystemTrackBuilder;
import engineer.fernando.cms.tracks.handler.model.TrackTacticalSituationBuilder;
import engineer.fernando.cms.tracks.handler.model.dao.TrackDAO;
import engineer.fernando.cms.tracks.handler.model.dao.TrackDAOBuilder;
import engineer.fernando.cms.tracks.handler.model.dao.TrackPerformanceDAO;
import engineer.fernando.cms.tracks.handler.model.dao.TrackTacticalUpdateDAO;
import engineer.fernando.cms.tracks.handler.model.dao.TrackTacticalUpdateDAOBuilder;
import engineer.fernando.cms.tracks.handler.model.entity.SystemTrack;
import engineer.fernando.cms.tracks.handler.model.entity.TrackSpecification;
import engineer.fernando.cms.tracks.handler.model.entity.TrackTacticalSituation;
import engineer.fernando.cms.tracks.handler.repository.SystemTrackRepository;
import engineer.fernando.cms.tracks.handler.repository.TrackTacticalSituationRepository;
import engineer.fernando.cms.tracks.handler.util.TrackSpecificationBundle;
import engineer.fernando.cms.tracks.handler.util.TacticalTools;
import org.apache.avro.generic.GenericRecord;
import org.apache.commons.lang3.tuple.Pair;
import org.geotools.referencing.GeodeticCalculator;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import javax.transaction.Transactional;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import static engineer.fernando.cms.tracks.handler.util.TrackSpecificationBundle.NOISE_SEA_LEVEL;

@Service
@Transactional
public class SystemTrackService {

    private static final long UPDATE_RATE = 2000;

    @Autowired
    private SystemTrackRepository systemTrackRepository;

    @Autowired
    private TrackTacticalSituationRepository systemTrackTacticalSituationRepository;

    @Autowired
    private KafkaService kafkaService;


    private static final Logger LOGGER = LoggerFactory.getLogger(SystemTrackService.class);
    private static final String MISSING_TACTICALUPDATE = "[%s] - SystemTrack have no tactical data. (Newly created?).";
    private static final String MISSING_TRACK = "[%s] - Track not found.";

    private Flux<TrackDAO> trackDAOFlux;

    private Flux<SystemTrack> systemTracksFlux;

    private final Map<String,List<Detection>> tracksDetections = new HashMap<>();

    public void loadSystemTracksFlux(){
        systemTracksFlux = Flux.fromIterable(systemTrackRepository.findAll());
    }

    public Flux<SystemTrack> filterSystemTracksFluxById(String trackId) {
        return systemTracksFlux.filter(systemTrack -> systemTrack.getSystemTrackId().equals(trackId));
    }

    public Flux<TrackDAO> getSystemTrackDAOFlux() {
        return trackDAOFlux;
    }

    public Map<String, List<Detection>> getTracksDetections() {
        return tracksDetections;
    }

    /**
     * Create a complete SystemTrack with Specification and first Tactical Update, from received Track avro object.
     * @param receivedTrack the received track in Track Avro idl schema
     * @return the SystemTrack created
     */
    private SystemTrack initSystemTrack(Track receivedTrack) {
        TrackSpecification trackSpecification = TrackSpecificationBundle.buildTrackSpecification(receivedTrack.getSensorType());
        SystemTrack systemtrack = new SystemTrackBuilder()
                .setPlayerId(receivedTrack.getPlayerId()).setSensorTrackType(receivedTrack.getSensorType())
                .setName(receivedTrack.getName().toString())
                .setWeaponsAvailable(trackSpecification.getWeaponsCapacity())
                .setOrderedSpeedOverGround(receivedTrack.getOrderedSpeedOverGround())
                .setOrderedCourseOverGround(receivedTrack.getOrderedCourseOverGround())
                .setTrackTechSpec(trackSpecification).setHit(false)
                .setSystemTrackId(getNextId())
                .build();

        new TrackTacticalSituationBuilder().setLatitude(receivedTrack.getPosition().getLatitude())
                .setLongitude(receivedTrack.getPosition().getLongitude())
                .setCourseOverGround(receivedTrack.getOrderedCourseOverGround())
                .setSpeedOverGround(0.0)
                .setdBNoise(0.0)
                .setFuelInGallons(trackSpecification.getFuelCapacityInGallons()).setSystemTrack(systemtrack).build();
        return systemtrack;
    }

    private void systemTracksFluxUpdate(SystemTrack systemTrack) {
        if(systemTrack.isHit()){
            if(LOGGER.isTraceEnabled()){
                LOGGER.trace(String.format("[%s] - Targeted SystemTrack will not be updated.",systemTrack.getSystemTrackId()));
            }
            return;
        }

        //I - Retrieve track and track last tactical update
        TrackTacticalSituation lastUpdate = systemTrack.getLastTacticalUpdate();
        if(lastUpdate == null){
            if(LOGGER.isErrorEnabled()){
                LOGGER.error(String.format(MISSING_TRACK,systemTrack.getSystemTrackId()));
            }
            return;
        }

        //II - Check if Track was targeted
        if(considerTrackTargeted(systemTrack)){
            return;
        }

        //III - Check if Track nas no fuel
        if(considerNoFuelTrack(systemTrack, lastUpdate)){
            return;
        }

        // IV - Compute tactical information updates
        long updateRateInSeconds = TimeUnit.MILLISECONDS.toSeconds(UPDATE_RATE);
        Pair<Double,Double> currentLatLong = Pair.of(lastUpdate.getLatitude(), lastUpdate.getLongitude());
        Pair<Double,Double> currentCourseAndSpeed =
            Pair.of(lastUpdate.getCourseOverGround(), lastUpdate.getSpeedOverGround());
        Pair<Point2D, Double> tacticalProjection =
            TacticalTools.computeProjectionAndDistance(
                currentLatLong,
                currentCourseAndSpeed,
                updateRateInSeconds);
        // Distance travelled in a period of after update rate.
        double distanceTravelled = tacticalProjection.getRight();
        Point2D futurePoint = tacticalProjection.getLeft();

        //V - Use TrackPerformanceDAO object to compute tactical updates:
        TrackPerformanceDAO trackPerformanceDAO =
            new TrackPerformanceDAO(systemTrack, lastUpdate, (int) updateRateInSeconds);
        double updatedFuelLevel = trackPerformanceDAO.getNextFuelInGallons();

        //VI - Compute future Speed Over Ground after update rate, considering acceleration factor and its noise.
        double updatedSpeed = trackPerformanceDAO.getNextSpeed();
        double selfNoiseInDb = trackPerformanceDAO.computeSelfNoise(updatedSpeed);

        //VII - Compute future Course Over Ground after update rate, considering angular acceleration factor
        double updatedCourse = trackPerformanceDAO.getNextCourse();

        double updatedDistance = lastUpdate.getDistanceTravelled() + distanceTravelled;
        // VIII - Squash history: If navigating with ordered speed & course, update last update instead add a new update
        if(lastUpdate.getSpeedOverGround() == systemTrack.getOrderedSpeedOverGround()
                && lastUpdate.getCourseOverGround() == systemTrack.getOrderedCourseOverGround() ){
            lastUpdate.setLatitude((float) futurePoint.getY());
            lastUpdate.setLongitude((float) futurePoint.getX());
            lastUpdate.setdBNoise(selfNoiseInDb);
            lastUpdate.setFuelInGallons(updatedFuelLevel);
            lastUpdate.setDistanceTravelled(updatedDistance);
            lastUpdate.setCreationTime(System.currentTimeMillis());
            systemTrack.setLastTacticalUpdate(lastUpdate);
        }
        else{
            // IX - When maneuvering Create and persist the new update after all tactical computations.
            new TrackTacticalSituationBuilder().setSystemTrack(systemTrack)
                    .setLatitude((float) futurePoint.getY()).setLongitude((float) futurePoint.getX())
                    .setCourseOverGround(updatedCourse).setSpeedOverGround(updatedSpeed).setdBNoise(selfNoiseInDb)
                    .setFuelInGallons(updatedFuelLevel).setDistanceTravelled(updatedDistance).build();
            if(LOGGER.isDebugEnabled()){
                LOGGER.debug(String.format("[%s] - Under Maneuver: {[Lat/Lng](%s) | [CoG] (%s->%s) | [SoG] (%s->%s)}",
                        systemTrack.getSystemTrackId(),
                        TacticalTools.formatLatLng(
                                systemTrack.getLastTacticalUpdate().getLatitude(),
                                systemTrack.getLastTacticalUpdate().getLongitude()
                        ),
                        systemTrack.getLastTacticalUpdate().getCourseOverGround(),
                        systemTrack.getOrderedCourseOverGround(),
                        systemTrack.getLastTacticalUpdate().getSpeedOverGround(),
                        systemTrack.getOrderedSpeedOverGround())
                );
            }
        }
        //X - Save the updated track
        systemTrackRepository.save(systemTrack);
    }

    /**
     * Consider the track detections to evaluate if track is targeted and processes it.
     * Temporary track fired rule definition: If any detection (except own launching) is closer than safety distance
     * @param systemTrack
     */
    private boolean considerTrackTargeted(SystemTrack systemTrack) {
        List<Detection> detections = computeDetections(systemTrack);
        for (Detection detection: detections) {
            // TODO: Implement rule to define if was target or not.
            // For now, If any detection (except itself and itself launching) is closer than safety distance, the ship is fired
            if( !detection.getTrackIdSource().equals(systemTrack.getSystemTrackId())
                    && !detection.getTrackIdSource().contains(systemTrack.getSystemTrackId())
                    && !systemTrack.getSystemTrackId().contains(detection.getTrackIdSource())
                    && !detection.isMySources()
                    && (detection.getDistance() < TrackSpecificationBundle.SAFETY_DISTANCE)){
                fireTrack(systemTrack);
                return true;
            }
        }
        return false;
    }

    /**
     * Process the fire for a targeted track
     * @param systemTrack
     */
    private void fireTrack(SystemTrack systemTrack) {
        // Ship hit: Fuel Lost , No speed.
        //Need to set a residual noise for a targeted ship to force weapon to be hit also
        // TODO: To avoid the behaviour above we need a SeaServer component to handle the maritime area events.
        TrackTacticalSituation lastUpdate = systemTrack.getLastTacticalUpdate();
        if(lastUpdate == null){
            if(LOGGER.isErrorEnabled()){
                LOGGER.error(String.format(MISSING_TACTICALUPDATE, systemTrack) );
            }
            return;
        }

        // I - Hit the track
        systemTrack.setHit(true);
        // II - Create the new update after hit and associate it to the track
        new TrackTacticalSituationBuilder().setSystemTrack(systemTrack)
                .setLatitude(lastUpdate.getLatitude()).setLongitude(lastUpdate.getLongitude())
                .setCourseOverGround(lastUpdate.getCourseOverGround())
                .setSpeedOverGround(0.0).setdBNoise(1).setFuelInGallons(0).build();
        //III - Save the updated track
        systemTrackRepository.save(systemTrack);
        if(LOGGER.isInfoEnabled()){
            LOGGER.info(String.format("[%s] - SystemTrack fired!",systemTrack.getSystemTrackId()));
        }
    }

    /**
     * Process the absense of fuel for a track
     * @param systemTrack
     * @param lastUpdate
     */
    private boolean considerNoFuelTrack(SystemTrack systemTrack, TrackTacticalSituation lastUpdate) {
        if(lastUpdate.getFuelInGallons() == 0){
            // Fuel Lost , No speed.
            //Need to set a residual noise (crew, electrical units) for a ship to force weapon to be hit also
            // I - Create the new update after fuel end and associate it to the track
            new TrackTacticalSituationBuilder().setSystemTrack(systemTrack)
                    .setLatitude(lastUpdate.getLatitude()).setLongitude(lastUpdate.getLongitude())
                    .setCourseOverGround(lastUpdate.getCourseOverGround())
                    .setSpeedOverGround(0.0).setdBNoise(1).setFuelInGallons(0).build();
            //III - Save the updated track
            systemTrackRepository.save(systemTrack);
            if(LOGGER.isInfoEnabled()){
                LOGGER.info(String.format("[%s] - SystemTrack finishes its fuel !",systemTrack.getSystemTrackId()));
            }
            return true;
        }
        return false;
    }

    /** Compute the Detections for the trackReference taking into account the detected noise at a given distance
     * @param trackReference
     * @return List of Detections for the given trackReference
     */
    public List<Detection> computeDetections(SystemTrack trackReference) {
        TrackTacticalSituation trackReferenceLastUpdate = trackReference.getLastTacticalUpdate();
        if(trackReferenceLastUpdate == null){
            if(LOGGER.isErrorEnabled()){
                LOGGER.error(String.format(MISSING_TACTICALUPDATE, trackReference.getSystemTrackId()) );
            }
            return Collections.emptyList();
        }

        List<Detection> detections = new ArrayList<>();
        systemTracksFlux.map(systemTrack ->
                detections.addAll(
                computeDetectionsBetweenTracks(
                        trackReference,
                        trackReferenceLastUpdate,
                        systemTrack
                    )
                )
        );
        detections.sort((o1, o2) -> (int) (o1.getDistance() - o2.getDistance()));
        tracksDetections.put(trackReference.getSystemTrackId(),detections);
        return detections;
    }

    /**
     *
     * @param trackReference The track used as reference
     * @param trackReferenceLastUpdate The last update fro track used as reference
     * @param track the another track used to comparison with the referenced track
     * @return the List of detections if the track in comparison is being detected by the referenced track or a empty list elsewhere
     */
    private List<Detection> computeDetectionsBetweenTracks(SystemTrack trackReference, TrackTacticalSituation trackReferenceLastUpdate, SystemTrack track) {
        List<Detection> detections = new ArrayList<>();
        if(trackReference.getSystemTrackId().equals(track.getSystemTrackId())){
            return Collections.emptyList();
        }
        //Get last tactical update
        TrackTacticalSituation trackLastUpdate = track.getLastTacticalUpdate();
        if(trackLastUpdate == null){
            if(LOGGER.isErrorEnabled()){
                LOGGER.error(String.format(MISSING_TACTICALUPDATE, track.getSystemTrackId()) );
            }
            return Collections.emptyList();
        }
        // Geodetic Calculation
        GeodeticCalculator gc = new GeodeticCalculator(DefaultGeographicCRS.WGS84);
        gc.setStartingGeographicPoint(trackReferenceLastUpdate.getLongitude(), trackReferenceLastUpdate.getLatitude());
        gc.setDestinationGeographicPoint(trackLastUpdate.getLongitude(), trackLastUpdate.getLatitude());
        double azimuth = gc.getAzimuth() - TacticalTools.courseInDegreesToAzimuth(trackReferenceLastUpdate.getCourseOverGround());
        double distance = gc.getOrthodromicDistance();
        double detectedNoiseInDb = trackLastUpdate.getdBNoise() + NOISE_SEA_LEVEL - distance * TrackSpecificationBundle.NOISE_PER_METERS_ATTENUATION_COEF;
        if(detectedNoiseInDb > 0 && 2* Math.abs(azimuth) <= trackReference.getTrackSpecification().getSonarDetectionAzimuthRange()){
            Detection d = new Detection(track.getSystemTrackId(), trackLastUpdate.getLatitude(),trackLastUpdate.getLongitude(),distance, azimuth,detectedNoiseInDb);
            // To avoid be killed by an own launched weapon due to the current proximity criteria to define a fired.
            if (track.getSensorTrackType().equals(SensorTrackType.WEAPON) && track.getSystemTrackId().contains(String.valueOf(trackReference.getSystemTrackId()))){
                d.setMySources(true);
            }
            if(LOGGER.isInfoEnabled()){
                LOGGER.info(String.format("[%s] - Sonar detection: %s!",trackReference.getSystemTrackId(), d));
            }
            detections.add(d);
        }
        return detections;
    }

    public void deleteAll() {
        systemTrackRepository.deleteAll();
    }

    public SystemTrack save(SystemTrack track) {
        return systemTrackRepository.save(track);
    }

    public BigDecimal getNextId(){
        return Optional.ofNullable(systemTrackRepository.getNextId()).orElse(BigDecimal.valueOf(1));
    }

    /**
     * Convert a SystemTrack from entity format to TrackDAO format
     * @param systemTrack the given systemTrack
     * @return the track in DAO format
     */
    public TrackDAO convertSystemTrackToTrackDAO(SystemTrack systemTrack) {
        TrackTacticalSituation lastUpdate = systemTrack.getLastTacticalUpdate();
        if(lastUpdate == null){
            if(LOGGER.isErrorEnabled()){
                LOGGER.error(String.format(MISSING_TACTICALUPDATE,systemTrack.getSystemTrackId()));
            }
            return null;
        }
        return new TrackDAOBuilder()
                .setPlayerId(systemTrack.getPlayerId())
                .setHit(systemTrack.isHit())
                .setCourseOverGround(lastUpdate.getCourseOverGround())
                .setName(systemTrack.getName())
                .setOrderedCourseOverGround(systemTrack.getOrderedCourseOverGround())
                .setFuelInGallons(lastUpdate.getFuelInGallons())
                .setNoiseInDb(lastUpdate.getdBNoise())
                .setWeaponsAvailable(systemTrack.getWeaponsAvailable())
                .setOrderedSpeedOverGround(systemTrack.getOrderedSpeedOverGround())
                .setTrackId(systemTrack.getSystemTrackId())
                .setLatitude(lastUpdate.getLatitude())
                .setLongitude(lastUpdate.getLongitude())
                .setDistanceTravelled(lastUpdate.getDistanceTravelled())
                .setSpeedOverGround(lastUpdate.getSpeedOverGround())
                .setRadarAntennaTurnStatus(false)
                .setRadarStatus(false)
                .setRadarAzimuth(0.0)
                .setSensorTrackType(systemTrack.getSensorTrackType())
                .setCreationTime(systemTrack.getCreationTime())
                .setUpdatedTime(lastUpdate.getCreationTime())
                .build();
    }

    /**
     * Get the list of tactical updates in DAO format from a given SystemTrack
     * @param trackId the given track Id
     * @return the list of tactical updates in DAO format
     */
    public List<TrackTacticalUpdateDAO> getTacticalUpdatesByTrackId(String trackId) {
        List<TrackTacticalUpdateDAO> result = new ArrayList<>();
        filterSystemTracksFluxById(trackId)
                .subscribe(
                    systemTrack -> {
                        List<TrackTacticalSituation> entityTacticalUpdates = systemTrack.getTacticalUpdates();
                        entityTacticalUpdates.sort(Comparator.comparing(TrackTacticalSituation::getCreationTime).reversed());
                        for (TrackTacticalSituation entityTacticalUpdate:entityTacticalUpdates) {
                            result.add(convertTrackTacticalSituationToTrackTacticalUpdateDAO(entityTacticalUpdate));
                        }
                    },
                    err -> {
                        if(LOGGER.isErrorEnabled()){
                            LOGGER.error(String.format(MISSING_TRACK,trackId), err);
                        }
                    });
        return result;
    }

    /**
     * Convert a TrackTacticalSituation update from entity format to TrackTacticalUpdateDAO format
     * @param entityUpdate the tactical update in the JPA entity format
     * @return the TrackTacticalUpdateDAO update
     */
    private static TrackTacticalUpdateDAO convertTrackTacticalSituationToTrackTacticalUpdateDAO(TrackTacticalSituation entityUpdate) {
        return new TrackTacticalUpdateDAOBuilder().setTrackId(entityUpdate.getSystemTrack().getSystemTrackId())
                .setLatitude(entityUpdate.getLatitude())
                .setLongitude(entityUpdate.getLongitude())
                .setCourseOverGround(entityUpdate.getCourseOverGround())
                .setSpeedOverGround(entityUpdate.getSpeedOverGround())
                .setNoiseInDb(entityUpdate.getdBNoise())
                .setFuelInGallons(entityUpdate.getFuelInGallons())
                .setCreationTime(entityUpdate.getCreationTime()).build();
    }

    public TrackSpecification getTrackSpecificationByTrackId(String trackId) {
        AtomicReference<TrackSpecification> specificationAtomicReference = new AtomicReference<>();
        filterSystemTracksFluxById(trackId)
                .subscribe(
                systemTrack -> specificationAtomicReference.set(systemTrack.getTrackSpecification()),
                err -> {
                    if(LOGGER.isErrorEnabled()){
                        LOGGER.error(String.format(MISSING_TRACK,trackId), err);
                    }
                    specificationAtomicReference.set(null);
                },
                () -> { /* TrackSpecification generated - Nothing to do */ }
        );
        return specificationAtomicReference.get();
    }

    public TrackPerformanceDAO getTrackPerformanceDAOByTrackId(String trackId) {
        AtomicReference<TrackPerformanceDAO> performanceDAOAtomicReference = new AtomicReference<>();
        filterSystemTracksFluxById(trackId)
                .subscribe(
                        systemTrack -> {
                            TrackTacticalSituation lastUpdate = systemTrack.getLastTacticalUpdate();
                            TrackPerformanceDAO trackPerformance =
                                    new TrackPerformanceDAO(systemTrack, lastUpdate, 5);
                            performanceDAOAtomicReference.set(trackPerformance);
                        },
                        err -> {
                            if(LOGGER.isErrorEnabled()){
                                LOGGER.error(String.format(MISSING_TRACK,trackId), err);
                            }
                            performanceDAOAtomicReference.set(null);
                        },
                        () -> { /* TrackPerformanceDAO generated - Nothing to do */ }
                );
        return performanceDAOAtomicReference.get();
    }

    public boolean launchWeapon(SystemTrack parentTrack) {
        TrackTacticalSituation lastTrackUpdate = parentTrack.getLastTacticalUpdate();
        if(lastTrackUpdate == null){
            if(LOGGER.isErrorEnabled()){
                LOGGER.error(String.format(MISSING_TACTICALUPDATE,parentTrack.getSystemTrackId()));
            }
            return false;
        }
        // Build specification from Weapon template.
        TrackSpecification weaponSpecification = TrackSpecificationBundle.buildTrackSpecification(SensorTrackType.WEAPON);
        SystemTrack systemtrack = new SystemTrackBuilder()
                .setPlayerId(parentTrack.getPlayerId()).setSensorTrackType(SensorTrackType.WEAPON)
                .setName(parentTrack.getName().concat("-GUN"))
                .setWeaponsAvailable(weaponSpecification.getWeaponsCapacity())
                .setOrderedSpeedOverGround(weaponSpecification.getMaxSpeedOverGround())
                .setOrderedCourseOverGround(lastTrackUpdate.getCourseOverGround())
                .setTrackTechSpec(weaponSpecification).setHit(false)
                .setSystemTrackId(getNextId(),parentTrack.getSystemTrackId())
                .build();

        new TrackTacticalSituationBuilder().setSystemTrack(systemtrack)
                .setLatitude(lastTrackUpdate.getLatitude())
                .setLongitude(lastTrackUpdate.getLongitude())
                .setCourseOverGround(lastTrackUpdate.getCourseOverGround())
                .setSpeedOverGround(0.0)
                .setdBNoise(0.0)
                .setFuelInGallons(weaponSpecification.getFuelCapacityInGallons()).build();
        save(systemtrack);
        if(LOGGER.isInfoEnabled()){
            LOGGER.info(String.format("[%s] -  Received SystemTrack (WEAPON): %s",
                systemtrack.getSystemTrackId(), systemtrack.getName()));
        }
        String alertMsg = String.format("Launched %s %s", systemtrack.getTrackSpecification().getTrackType(), systemtrack.getName());
        kafkaService.sendAlertMessage(systemtrack.getSystemTrackId(), alertMsg);
        return true;
    }

    public boolean sendCommand(String trackId, String param, float value) {
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        AtomicReference<String> alertCmd = new AtomicReference<>();
        filterSystemTracksFluxById(trackId)
                .subscribe(systemTrack -> {
                        try{
                            switch (param){
                                case "SoG":
                                    systemTrack.setOrderedSpeedOverGround(value);
                                    atomicBoolean.set(true);
                                    alertCmd.set(String.format("Ordered Speed %s knots.", Math.round(value * 100.0) / 100.0));
                                    break;
                                case "CoG":
                                    systemTrack.setOrderedCourseOverGround(value);
                                    atomicBoolean.set(true);
                                    alertCmd.set(String.format("Ordered Course  %s deg.", Math.round(value * 100.0) / 100.0));
                                    break;
                                case "DEL":
                                    systemTrackTacticalSituationRepository.deleteBySystemTrack(systemTrack);
                                    systemTrackRepository.delete(systemTrack);
                                    alertCmd.set("Ordered Self Destruction");
                                    break;
                                case "FIRE":
                                    if(systemTrack.getWeaponsAvailable() < 1){
                                        alertCmd.set("No more Weapons available");
                                        if(LOGGER.isInfoEnabled()){
                                            LOGGER.info(alertCmd.get());
                                        }
                                        break;
                                    }
                                    if(launchWeapon(systemTrack)){
                                        systemTrack.setWeaponsAvailable(systemTrack.getWeaponsAvailable() - 1);
                                        alertCmd.set(String.format("Remaining Weapons: %s",
                                            systemTrack.getWeaponsAvailable()));
                                        atomicBoolean.set(true);
                                    }
                                    break;
                                default:
                                    throw new TrackHandlerCommandException("Fatal: Options Should be existing ones (SoG/CoG/DEL/FIRE)");
                            }
                            if(atomicBoolean.get()){
                                save(systemTrack);
                            }
                            if(LOGGER.isInfoEnabled()){
                                LOGGER.info(String.format("[%s] - %s",systemTrack.getSystemTrackId(), alertCmd.get()));
                            }
                            kafkaService.sendAlertMessage(trackId,alertCmd.get());
                        }
                        catch (TrackHandlerCommandException e) {
                            if(LOGGER.isErrorEnabled()){
                                LOGGER.error(e.getMessage());
                            }
                        }
                } , err -> {
                            if(LOGGER.isErrorEnabled()){
                                LOGGER.error(String.format(MISSING_TRACK,trackId), err);
                            }
                        } , () -> {
                            if(LOGGER.isDebugEnabled()){
                                LOGGER.debug(String.format("[%s] - Ordered %s Completed.",trackId,param));
                            }
                        });
        return atomicBoolean.get();
    }

    public List<SystemTrack> launchTrack(byte[] data) throws IOException {
        List<SystemTrack> receivedSystemTracks = new ArrayList<>();
        List<GenericRecord> records = Codec.deserializeToAvroRecord(data);
        for (GenericRecord avroRecord: records) {
            if(LOGGER.isDebugEnabled()){
                LOGGER.debug(String.format("Processing record: %s ", avroRecord.toString()));
            }
            if (!(avroRecord instanceof Track)) {
                if(LOGGER.isErrorEnabled()){
                    LOGGER.error(String.format("Record %s is not instance of Track Schema: %s ", avroRecord.toString(), Track.getClassSchema().toString()));
                }
                continue;
            }
            Track receivedTrack = (Track) avroRecord;
            if(LOGGER.isDebugEnabled()){
                LOGGER.debug("Record converted into Track");
            }
            SystemTrack systemTrack = initSystemTrack(receivedTrack);
            save(systemTrack);
            if(LOGGER.isInfoEnabled()){
                LOGGER.info(String.format("[%s] -  Received SystemTrack: %s",systemTrack.getSystemTrackId(), systemTrack));
            }
            receivedSystemTracks.add(systemTrack);
        }
        return receivedSystemTracks;
    }

    public void consumeSystemTrack(Track receivedTrack){
        SystemTrack systemTrack = initSystemTrack(receivedTrack);
        save(systemTrack);
        if(LOGGER.isInfoEnabled()){
            LOGGER.info(String.format("[%s] -  Received SystemTrack from RabbitMQ: %s",systemTrack.getSystemTrackId(), systemTrack.getName()));
        }
        String alertNewTrack = String.format("RabbitMQ launch %s %s ",systemTrack.getTrackSpecification().getTrackType(), systemTrack.getName());
        kafkaService.sendAlertMessage(systemTrack.getSystemTrackId(),alertNewTrack);
    }

    @Async
    @Scheduled(initialDelay = 5000, fixedRate = UPDATE_RATE)
    protected void updateSystemTracks() {
        loadSystemTracksFlux();
        trackDAOFlux = systemTracksFlux.map(this::convertSystemTrackToTrackDAO);
        Optional<List<TrackDAO>> optionalTrackDAOListtrackDAOList = trackDAOFlux.collectList().blockOptional();
        if(optionalTrackDAOListtrackDAOList.isPresent()){
            List<TrackDAO> trackDAOList = optionalTrackDAOListtrackDAOList.get();
            // Send TrackDAO Map message regardless if it is empty (serves as Heartbit Message)
            kafkaService.sendTrackDAOMessage(trackDAOList);
            if(!trackDAOList.isEmpty()){
                List<TrackPerformanceDAO> trackPerformanceDAOList = new ArrayList<>();
                for (TrackDAO trackDAO: trackDAOList){
                    trackPerformanceDAOList.add(getTrackPerformanceDAOByTrackId(trackDAO.getTrackId()));
                }
                kafkaService.sendTrackPerformanceDAOMessage(trackPerformanceDAOList);
            }
            systemTracksFlux.subscribe(this::systemTracksFluxUpdate,
                this::systemTracksFluxOnError, this::systemTracksFluxOnComplete);
        }
    }

    private void systemTracksFluxOnComplete() {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Update of SystemTracks Flux Completed.");
        }
    }

    private void systemTracksFluxOnError(Throwable err) {
        if (LOGGER.isErrorEnabled()) {
            LOGGER.error("Error: ", err);
        }
    }
}
