package engineer.fernando.cms.tracks.handler.controller;

import engineer.fernando.cms.tracks.handler.model.Detection;
import engineer.fernando.cms.tracks.handler.model.dao.TrackDAO;
import engineer.fernando.cms.tracks.handler.model.dao.TrackPerformanceDAO;
import engineer.fernando.cms.tracks.handler.model.dao.TrackTacticalUpdateDAO;
import engineer.fernando.cms.tracks.handler.model.entity.SystemTrack;
import engineer.fernando.cms.tracks.handler.model.entity.TrackSpecification;
import engineer.fernando.cms.tracks.handler.service.SystemTrackService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
@RestController
@CrossOrigin
public class SystemTrackController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SystemTrackController.class);

    @Autowired
    private SystemTrackService systemTrackService;

    @GetMapping(value = "/track", produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<TrackDAO> getSystemTracks(@RequestParam(required = false) String trackId) {
        return systemTrackService.getSystemTrackDAOFlux();
    }

    @GetMapping("/track/{trackId}")
    public Mono<ResponseEntity<TrackDAO>> getTrackById(@PathVariable("trackId") String trackId) {
        return systemTrackService.getSystemTrackDAOFlux()
                .filter(trackDAO -> trackDAO.getTrackId().equals(trackId)).singleOrEmpty()
                .map(trackDAO -> ResponseEntity.status(HttpStatus.OK).body(trackDAO))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.NOT_FOUND).body(null)));
    }

    @GetMapping("/track/{trackId}/history")
    public Flux<TrackTacticalUpdateDAO> getTrackHistoryById(@PathVariable("trackId") String trackId) {
        return Flux.fromIterable(systemTrackService.getTacticalUpdatesByTrackId(trackId));
    }

    @GetMapping("/track/{trackId}/specification")
    public Mono<ResponseEntity<TrackSpecification>> getTrackSpecification(@PathVariable("trackId") String trackId) {
        return Mono.justOrEmpty(systemTrackService.getTrackSpecificationByTrackId(trackId))
                .map(trackSpecification -> ResponseEntity.status(HttpStatus.OK).body(trackSpecification))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.NOT_FOUND).body(null)));
    }

    @GetMapping("/track/{trackId}/performance")
    public Mono<ResponseEntity<TrackPerformanceDAO>> getTrackPerformance(@PathVariable("trackId") String trackId) {
        return Mono.justOrEmpty(systemTrackService.getTrackPerformanceDAOByTrackId(trackId))
                .map(trackPerformanceDAO -> ResponseEntity.status(HttpStatus.OK).body(trackPerformanceDAO))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.NOT_FOUND).body(null)));
    }

    @GetMapping(value = "/track/{trackId}/detections", produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<Detection> getTrackDetections(@PathVariable("trackId") String trackId) {
        if(!systemTrackService.getTracksDetections().containsKey(trackId)){
           return Flux.empty();
        }
        return Flux.fromIterable(systemTrackService.getTracksDetections().get(trackId));
    }

    @DeleteMapping("/tracks")
    public ResponseEntity<HttpStatus> deleteAllSystemTracks() {
        try {
            systemTrackService.deleteAll();
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            if(LOGGER.isErrorEnabled()){
                LOGGER.error(e.getMessage());
            }
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/track/{trackId}")
    public Mono<ResponseEntity<Boolean>> deleteSystemTracksById(@PathVariable("trackId") String trackId) {
        return systemTrackService.sendCommand(trackId, "DEL",1) ?
                Mono.just(ResponseEntity.ok(true)) : Mono.just(ResponseEntity.notFound().build());
    }

    @GetMapping("/track/{trackId}/sog/{sog}")
    public Mono<ResponseEntity<Boolean>> setOrderedSpeedOverGround(@PathVariable("trackId") String trackId,@PathVariable("sog") long speedOverGround) {
        return systemTrackService.sendCommand(trackId, "SoG",speedOverGround/100f) ?
                Mono.just(ResponseEntity.ok(true)) : Mono.just(ResponseEntity.notFound().build());
    }

    @GetMapping("/track/{trackId}/cog/{cog}")
    public Mono<ResponseEntity<Boolean>> setOrderedCourseOverGround(@PathVariable("trackId") String trackId,@PathVariable("cog") long courseOverGround) {
        return systemTrackService.sendCommand(trackId, "CoG",courseOverGround/100f) ?
                Mono.just(ResponseEntity.ok(true)) : Mono.just(ResponseEntity.notFound().build());
    }

    @GetMapping("/track/{trackId}/fire")
    public Mono<ResponseEntity<Boolean>> launchWeapon(@PathVariable("trackId") String trackId) {
        return systemTrackService.sendCommand(trackId, "FIRE",1) ?
                Mono.just(ResponseEntity.ok(true)) : Mono.just(ResponseEntity.notFound().build());
    }

    @PostMapping(value = "/track", consumes = MediaType.APPLICATION_OCTET_STREAM_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> postSystemTrack(@RequestBody byte[] data){
        try {
            List<SystemTrack> receivedSystemTracks = systemTrackService.launchTrack(data);
            return new ResponseEntity<>(receivedSystemTracks, HttpStatus.CREATED);
        }
        catch (Exception e){
            if(LOGGER.isErrorEnabled()){
                LOGGER.error(e.getMessage());
            }
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
