package engineer.fernando.cms.tracks.handler.model.dao;

import java.io.Serializable;

public class TrackTacticalUpdateDAO implements Serializable {

    private String trackId;
    private double latitude;
    private double longitude;
    private double speedOverGround;
    private double courseOverGround;
    private double fuelInGallons;
    private double noiseInDb;
    private long creationTime;

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getSpeedOverGround() {
        return speedOverGround;
    }

    public void setSpeedOverGround(double speedOverGround) {
        this.speedOverGround = speedOverGround;
    }

    public double getCourseOverGround() {
        return courseOverGround;
    }

    public void setCourseOverGround(double courseOverGround) {
        this.courseOverGround = courseOverGround;
    }

    public double getFuelInGallons() {
        return fuelInGallons;
    }

    public void setFuelInGallons(double fuelInGallons) {
        this.fuelInGallons = fuelInGallons;
    }

    public double getNoiseInDb() {return noiseInDb;}

    public void setNoiseInDb(double noiseInDb) {
        this.noiseInDb = noiseInDb;
    }


    public long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(long creationTime) { this.creationTime = creationTime;}

    @Override
    public String toString() {
        return "TrackTacticalUpdateDAO [TrackId=" + trackId + ", Lat/Lng=(" + latitude + "," + longitude+")"
                + ", CoG="+ courseOverGround + ", SoG=" +speedOverGround + ", FuelInGallons=" + fuelInGallons
                + ", NoiseInDb=" + noiseInDb + ", CreationTime=" +creationTime +".]";
    }


}
