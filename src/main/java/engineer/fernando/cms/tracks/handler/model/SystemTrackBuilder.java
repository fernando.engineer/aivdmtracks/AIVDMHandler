package engineer.fernando.cms.tracks.handler.model;

import engineer.fernando.cms.domainmodel.avro.SensorTrackType;
import engineer.fernando.cms.tracks.handler.model.entity.SystemTrack;
import engineer.fernando.cms.tracks.handler.model.entity.TrackSpecification;

import java.math.BigDecimal;


public class SystemTrackBuilder {

    private SensorTrackType sensorTrackType;
    private long playerId;
    private TrackSpecification specification;
    private String name;
    private double orderedSpeedOverGround;
    private double orderedCourseOverGround;
    private int weaponsAvailable;
    private boolean hit;
    private String systemTrackId;


    public SystemTrackBuilder setSystemTrackId(BigDecimal nextval) {
        this.systemTrackId = this.sensorTrackType.toString().substring(0,1).concat(nextval+"P"+this.playerId);
        return this;
    }

    public SystemTrackBuilder setSystemTrackId(BigDecimal nextval, String parentTrackId) {
        setSystemTrackId(nextval);
        this.systemTrackId = this.systemTrackId.concat("_").concat(parentTrackId);
        return this;
    }

    public SystemTrackBuilder setSensorTrackType(SensorTrackType sensorTrackType) {
        this.sensorTrackType = sensorTrackType;
        return this;
    }

    public SystemTrackBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public SystemTrackBuilder setPlayerId(long playerId) {
        this.playerId = playerId;
        return this;
    }

    public SystemTrackBuilder setTrackTechSpec(TrackSpecification spec) {
        this.specification = spec;
        return this;
    }

    public SystemTrackBuilder setOrderedCourseOverGround(double orderedCourseOverGround) {
        this.orderedCourseOverGround = orderedCourseOverGround;
        return this;
    }

    public SystemTrackBuilder setOrderedSpeedOverGround(Double orderedSpeedOverGround) {
        this.orderedSpeedOverGround = orderedSpeedOverGround;
        return this;
    }

    public SystemTrackBuilder setWeaponsAvailable(int weaponsAvailable) {
        this.weaponsAvailable = weaponsAvailable;
        return this;
    }

    public SystemTrackBuilder setHit(boolean hit) {
        this.hit = hit;
        return this;
    }

    public SystemTrack build(){
        SystemTrack track = new SystemTrack();
        track.setSystemTrackId(this.systemTrackId);
        track.setSensorTrackType(this.sensorTrackType);
        track.setName(this.name);
        track.setPlayerId(this.playerId);
        track.setTrackSpecification(specification);
        track.setCreationTime(System.currentTimeMillis());
        track.setOrderedSpeedOverGround(this.orderedSpeedOverGround);
        track.setOrderedCourseOverGround(this.orderedCourseOverGround);
        track.setWeaponsAvailable(this.weaponsAvailable);
        track.setHit(this.hit);
        return track;
    }
}
