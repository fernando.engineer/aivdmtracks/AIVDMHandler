package engineer.fernando.cms.tracks.handler.repository;

import engineer.fernando.cms.tracks.handler.model.entity.AISTrackMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface AISTrackMessageRepository extends JpaRepository<AISTrackMessage,Long>  {

	List<AISTrackMessage> findByMmsi(long mmsi);

	@Transactional
	@Modifying
	@Query("DELETE FROM AISTrackMessage c WHERE c.mmsi = :mmsi")
	void deleteByMmsi(long mmsi);
}
