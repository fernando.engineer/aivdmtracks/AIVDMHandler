package engineer.fernando.cms.tracks.handler.service;

import engineer.fernando.cms.tracks.handler.messages.TrackHandlerMessage;
import engineer.fernando.cms.tracks.handler.model.dao.AISTrackInfoDAO;
import engineer.fernando.cms.tracks.handler.model.dao.TrackDAO;
import engineer.fernando.cms.tracks.handler.model.dao.TrackPerformanceDAO;
import reactor.core.publisher.Flux;
import reactor.kafka.receiver.ReceiverRecord;

import java.util.List;

public interface KafkaService {

    Flux<ReceiverRecord<String, TrackHandlerMessage>> getKafkaStream();

    void sendAlertMessage(String trackId, String message);

    void sendTrackDAOMessage(List<TrackDAO> trackDAOList);

    void sendTrackPerformanceDAOMessage(List<TrackPerformanceDAO> trackPerformanceDAOList);

    void sendAISTrack(AISTrackInfoDAO aisTrackInfo);

}