package engineer.fernando.cms.tracks.handler.messages;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class KafkaJsonDeserializer implements Deserializer<TrackHandlerMessage> {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaJsonDeserializer.class);

    @Override
    public void configure(Map map, boolean b) {
        // Nothing to Do but Serializer interface requires it.
    }

    @Override
    public TrackHandlerMessage deserialize(String s, byte[] bytes) {
        ObjectMapper mapper = new ObjectMapper();
        TrackHandlerMessage obj = null;
        try {
            obj = mapper.readValue(bytes, TrackHandlerMessage.class);
        } catch (Exception e) {
            if(LOGGER.isErrorEnabled()){
                LOGGER.error("Error: ", e);
            }
        }
        return obj;
    }

    @Override
    public void close() {
        // Nothing to Do but Serializer interface requires it.
    }
}