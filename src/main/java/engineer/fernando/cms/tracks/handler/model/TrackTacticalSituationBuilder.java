package engineer.fernando.cms.tracks.handler.model;

import engineer.fernando.cms.tracks.handler.model.entity.SystemTrack;
import engineer.fernando.cms.tracks.handler.model.entity.TrackTacticalSituation;

public class TrackTacticalSituationBuilder {

    private SystemTrack systemTrack;
    private double latitude;
    private double longitude;
    private double speedOverGround;
    private double courseOverGround;
    private double distanceTravelled;
    private double fuelInGallons;
    private double dBNoise;

    public TrackTacticalSituationBuilder setSystemTrack(SystemTrack systemTrack) {
        this.systemTrack = systemTrack;
        return this;
    }

    public TrackTacticalSituationBuilder setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public TrackTacticalSituationBuilder setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public TrackTacticalSituationBuilder setSpeedOverGround(double speedOverGround) {
        this.speedOverGround = speedOverGround;
        return this;
    }

    public TrackTacticalSituationBuilder setCourseOverGround(double courseOverGround) {
        this.courseOverGround = courseOverGround;
        return this;
    }

    public TrackTacticalSituationBuilder setDistanceTravelled(double distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
        return this;
    }

    public TrackTacticalSituationBuilder setFuelInGallons(double fuelInGallons)
    {
        this.fuelInGallons = fuelInGallons;
        return this;
    }

    public TrackTacticalSituationBuilder setdBNoise(double dBNoise) {
        this.dBNoise = dBNoise;
        return this;
    }

    public TrackTacticalSituation build(){
        TrackTacticalSituation tacticalSituation = new TrackTacticalSituation();
        tacticalSituation.setSystemTrack(this.systemTrack);
        tacticalSituation.setLatitude(this.latitude);
        tacticalSituation.setLongitude(this.longitude);
        tacticalSituation.setSpeedOverGround(this.speedOverGround);
        tacticalSituation.setCourseOverGround(this.courseOverGround);
        tacticalSituation.setDistanceTravelled(this.distanceTravelled);
        tacticalSituation.setFuelInGallons(this.fuelInGallons);
        tacticalSituation.setdBNoise(this.dBNoise);
        tacticalSituation.setCreationTime(System.currentTimeMillis());
        return tacticalSituation;
    }

}
