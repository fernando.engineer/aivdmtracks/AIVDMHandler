package engineer.fernando.cms.tracks.handler.util;

import engineer.fernando.cms.domainmodel.avro.SensorTrackType;
import engineer.fernando.cms.domainmodel.avro.TrackType;
import engineer.fernando.cms.tracks.handler.model.TrackSpecificationBuilder;
import engineer.fernando.cms.tracks.handler.model.entity.TrackSpecification;

public class TrackSpecificationBundle {

    private TrackSpecificationBundle(){
    }

    private static final double OWNSHIP_MAX_SPEED = 200;

    private static final double OWNSHIP_ACCELERATION = 1.5;
    private static final double OWNSHIP_ANGULAR_ACCELERATION = 0.8;
    private static final double OWNSHIP_DIM_LOA = 100;
    private static final double OWNSHIP_DIM_BEAM = 20;
    private static final double OWNSHIP_DIM_DRAUGHT = 10;
    private static final double OWNSHIP_DIM_GROSSTONNAGE = 10000;
    private static final double OWNSHIP_DIM_NETTONNAGE = 5000;
    private static final double OWNSHIP_FUEL_CAPACITY_IN_GALLONS = 1000000;

    //Inspired in https://www.baesystems.com/en/product/s1850m-long-range-radar
    private static final double OWNSHIP_RADAR_ANTENNA_TURN_SPEED = 180;
    private static final double OWNSHIP_RADAR_SENSIBILITY_AZIMUTH = 3;
    private static final double OWNSHIP_RADAR_RANGE = 400000;
    private static final double OWNSHIP_RADAR_PULSE_POWER = 300;
    private static final int OWNSHIP_WEAPON_STORAGE = 5;

    private static final double WEAPON_ACCELERATION = 100;
    private static final double WEAPON_ANGULAR_ACCELERATION = 7;
    private static final double WEAPON_DIM_LOA = 5;
    private static final double WEAPON_DIM_BEAM = 1;
    private static final double WEAPON_DIM_DRAUGHT = 1;
    private static final double WEAPON_DIM_GROSSTONNAGE = 1.5;
    private static final double WEAPON_DIM_NETTONNAGE = 1;
    private static final double WEAPON_FUEL_CAPACITY_IN_GALLONS = 500;
    private static final double WEAPON_RANGE = 2000000;

    // Max Speed Inspired by BrahMos-II https://en.wikipedia.org/wiki/BrahMos-II  hypersonic cruise missile
    private static final double WEAPON_MAX_SPEED = 10500;

    private static final double WEAPON_SENSIBILITY_AZIMUTH_DB = 60;
    private static final double WEAPON_AUTOGUIDED_SENSIBILITY_AZIMUTH_DB = 30;
    private static final double WEAPON_AUTOGUIDED_GAIN_AZIMUTH_DB = 3;

    public static final double LITERS_PER_GALLON = 3.78541;
    private static final double OWNSHIP_SENSIBILITY_AZIMUTH_DB = 240;

    public static final double NOISE_SEA_LEVEL = 20;

    //TODO: Noise coef shall be specification attributes
    public static final double OWNSHIP_NOISE_PER_SPEED_LINEAR_COEF = 0.1f;
    public static final double OWNSHIP_NOISE_PER_SPEED_QUAD_COEF = 0.0001f;
    public static final double WEAPON_NOISE_PER_SPEED_LINEAR_COEF = 0.005f;
    public static final double WEAPON_NOISE_PER_SPEED_QUAD_COEF = 0.0000001f;

    public static final double WEAPON_MIN_SPEED = 500;
    public static final double NOISE_PER_METERS_ATTENUATION_COEF = 0.0004f;
    public static final double DETECTION_RANGE = NOISE_SEA_LEVEL / NOISE_PER_METERS_ATTENUATION_COEF;
    public static final double SAFETY_DISTANCE = 200;
    public static final double ATTACK_DISTANCE = 50000;

    public static TrackSpecification buildTrackSpecification(SensorTrackType sensorTrackType){
        switch (sensorTrackType){
            case WEAPON:
                return new TrackSpecificationBuilder()
                        .setMaxSpeedOverGround(WEAPON_MAX_SPEED)
                        .setAcceleration(WEAPON_ACCELERATION)
                        .setAngularAcceleration(WEAPON_ANGULAR_ACCELERATION)
                        .setLoa(WEAPON_DIM_LOA).setBeam(WEAPON_DIM_BEAM)
                        .setDraught(WEAPON_DIM_DRAUGHT).setGrossTonnage(WEAPON_DIM_GROSSTONNAGE)
                        .setNetTonnage(WEAPON_DIM_NETTONNAGE)
                        .setFuelCapacityInGallons(WEAPON_FUEL_CAPACITY_IN_GALLONS)
                        .setRadarAntennaTurnSpeed(OWNSHIP_RADAR_ANTENNA_TURN_SPEED)
                        .setRadarDetectionAzimuthRange(OWNSHIP_RADAR_SENSIBILITY_AZIMUTH)
                        .setRadarDetectionRange(OWNSHIP_RADAR_RANGE).setRadarTransmittedPulsePower(OWNSHIP_RADAR_PULSE_POWER)
                        .setSonarDetectionRange(DETECTION_RANGE).setSonarDetectionAzimuthRange(WEAPON_SENSIBILITY_AZIMUTH_DB)
                        .setDirectivityDetectionGain(1.0)
                        .setDirectivityDetectionAzimuth(360.0)
                        .setNoiseDueSpeedLinearCoefficient(WEAPON_NOISE_PER_SPEED_LINEAR_COEF)
                        .setNoiseDueSpeedQuadraticCoefficient(WEAPON_NOISE_PER_SPEED_QUAD_COEF)
                        .setWeaponsCapacity(0).setTrackType(TrackType.WEAPON).build();
            case OWNSHIP: default:
                return new TrackSpecificationBuilder()
                        .setMaxSpeedOverGround(OWNSHIP_MAX_SPEED)
                        .setAcceleration(OWNSHIP_ACCELERATION)
                        .setAngularAcceleration(OWNSHIP_ANGULAR_ACCELERATION)
                        .setLoa(OWNSHIP_DIM_LOA).setBeam(OWNSHIP_DIM_BEAM)
                        .setDraught(OWNSHIP_DIM_DRAUGHT).setGrossTonnage(OWNSHIP_DIM_GROSSTONNAGE)
                        .setNetTonnage(OWNSHIP_DIM_NETTONNAGE)
                        .setFuelCapacityInGallons(OWNSHIP_FUEL_CAPACITY_IN_GALLONS)
                        .setRadarAntennaTurnSpeed(OWNSHIP_RADAR_ANTENNA_TURN_SPEED)
                        .setRadarDetectionAzimuthRange(OWNSHIP_RADAR_SENSIBILITY_AZIMUTH)
                        .setRadarDetectionRange(OWNSHIP_RADAR_RANGE).setRadarTransmittedPulsePower(OWNSHIP_RADAR_PULSE_POWER)
                        .setSonarDetectionRange(DETECTION_RANGE).setSonarDetectionAzimuthRange(OWNSHIP_SENSIBILITY_AZIMUTH_DB)
                        .setDirectivityDetectionGain(1.0)
                        .setDirectivityDetectionAzimuth(360.0)
                        .setNoiseDueSpeedLinearCoefficient(OWNSHIP_NOISE_PER_SPEED_LINEAR_COEF)
                        .setNoiseDueSpeedQuadraticCoefficient(OWNSHIP_NOISE_PER_SPEED_QUAD_COEF)
                        .setWeaponsCapacity(OWNSHIP_WEAPON_STORAGE).setTrackType(TrackType.NAVALSHIP).build();
        }
    }





}
