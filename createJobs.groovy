pipelineJob('pipelineJob') {
    definition {
        cps {
            script(readFileFromWorkspace('pipelineJob.groovy'))
            sandbox()
        }
    }
}
pipelineJob('TrackHandler-job') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url 'https://gitlab.com/fjcapeletto/TrackHandler.git'
                    }
                    branch '3.0'
                }
            }
        }
    }
}
pipelineJob('TrackHandler-job-docker') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url 'https://gitlab.com/fjcapeletto/TrackHandler.git'
                    }
                    branch '3.0'
                    scriptPath('Jenkinsfile-docker')
                }
            }
        }
    }
}
pipelineJob('TrackHandler-job-aws') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url 'https://gitlab.com/fjcapeletto/TrackHandler.git'
                    }
                    branch '3.0'
                    scriptPath('Jenkinsfile-aws')
                }
            }
        }
    }
}
